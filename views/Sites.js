import React, {Component} from 'react';
import { ScrollView, Text, StyleSheet, Button, Alert, View, ActivityIndicator,FlatList, SectionList} from 'react-native';

class Sites extends Component {

  constructor(props){
    super(props);
    this.state ={ 
      isLoading: true,
      counter: 0
    }
  }

  componentDidMount() {
    this.makeRemoteRequest();
  }

  makeRemoteRequest = () => {
    const url = 'https://api.cashcomplete.com/api/v1/machines';
    return fetch(url, {
      method: 'GET',
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": "Basic ZGV2ZWxvcGVyQHB1cnBsZXNjb3V0LnNlOlBTMTM1Nzk=",
        "X-SiteWhere-Tenant": "suzo123",
        "Authorization":"Bearer 68a89a19-1ce8-309c-b764-d21208d5da02"
      },
    })
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({
        isLoading: false,
        dataSource: responseJson,
      }, function(){
        
      });
    })
    .catch((error) => {
      console.error("ERROR:" + error);
    });
  };

  changeCounter(val) {
    this.setState(prevState => ({
      counter: prevState.counter + val
    }));
  }

  addOne() {
    this.changeCounter(1);
  }

  removeOne() {
    this.changeCounter(-1);
  }

  renderHeader = () => {
    return <SearchBar placeholder="Type Here..." lightTheme round />;
  };

  render() {

    if (this.state.isLoading) {
      return(
        <View style={{flex: 1, padding: 20,justifyContent: 'center',
        alignItems: 'center'}}>
          <ActivityIndicator/>
        </View>
      )
    }
    
    return (

      // <View>
      //   <SectionList
      //     sections={this.state.dataSource}
      //     renderItem={
      //       ({item}) => <Text>{item.status.errorStatus}</Text>
      //     }
      //     renderSectionHeader={
      //       ({section}) => <Text>{item.machineId}</Text>
      //     }
      //     keyExtractor={(item, index) => index}
      //   >
      //   </SectionList>
      // </View>

    <View style={{flex: 1, paddingTop:20}}>
        <FlatList
          style={{flex:1, paddingTop: 10}}
          data={this.state.dataSource.results}
          renderItem={({item}) => <Text style={{padding: 5, fontSize: 15, color: "purple", borderBottomColor:"black", borderWidth: 0.5}}>Alias: {item.sitewhereDevice.metadata.alias}, Status: {item.status.status}</Text>}
          keyExtractor={(item, index) => index}
        />

        <Text>Counter: {this.state.counter}</Text>
        <Button title="Add one" onPress={() => this.addOne()}/>
        <Button title="Remove one" onPress={() => this.removeOne()}/>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  textSites: {
    fontSize: 50,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 300,
  },
});
export default Sites;


